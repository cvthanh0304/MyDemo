package com.example.bobo.mydemo.UI.ModelUI;

public class MyItem {
    private String ID;
    private int value;

    public MyItem(String ID, int value) {
        this.ID = ID;
        this.value = value;
    }

    public String getID() {
        return ID;
    }

    public int getValue() {
        return value;

    }

    public void setValue(int value) {
        this.value = value;
    }
}
