package com.example.bobo.mydemo.UI.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.bobo.mydemo.Entity.Item;
import com.example.bobo.mydemo.Model.ItemModel;
import com.example.bobo.mydemo.UI.ModelUI.MyItem;
import com.example.bobo.mydemo.R;
import com.example.bobo.mydemo.Adapter.MyAdapter;
import com.example.bobo.mydemo.MyInterface;

import java.util.ArrayList;
import java.util.List;

public class FragmentTwo extends Fragment implements MyInterface, View.OnClickListener {

    Button confirmBtn;
    public TextView headerTextView;
    RecyclerView recyclerView;
    MyAdapter myAdapter;
    List<MyItem> itemList = new ArrayList<>();
    List<Item> updateList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_two, container, false);
        fragmentInitial(view);
        recyclerViewInitial();
        return view;
    }

    private void fragmentInitial(View view) {
        confirmBtn = view.findViewById(R.id.confirmBtn);
        recyclerView = view.findViewById(R.id.recyclerView);
        headerTextView = view.findViewById(R.id.headerTextView);
        confirmBtn.setOnClickListener(this);
    }

    private void recyclerViewInitial() {
        myAdapter = new MyAdapter(itemList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setAutoMeasureEnabled(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(myAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        addData();
        updateSum();
    }

    @Override
    public void updateSum() {
        headerTextView.setText(String.valueOf(myAdapter.getSum()));
    }

    private void addData() {
        ItemModel itemModel = new ItemModel(getContext());
        List<Item> listItem = itemModel.getAllItems();
        for (int i = 0; i < listItem.size(); i++) {
            MyItem item = new MyItem(listItem.get(i).getID(), listItem.get(i).getValue());
            itemList.add(item);
            Log.i("position", item.getID());
            Log.i("value", String.valueOf(item.getValue()));
        }
    }tu

    public void addUpdateList(MyItem item) {
        updateList.add(new Item(item.getID(), item.getValue()));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirmBtn:
                getFragmentManager().popBackStack();
                ItemModel itemModel = new ItemModel(getContext());
                itemModel.updateAllItem(updateList);
                break;

        }
    }
}
