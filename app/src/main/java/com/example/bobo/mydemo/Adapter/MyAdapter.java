package com.example.bobo.mydemo.Adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.bobo.mydemo.UI.ModelUI.MyItem;
import com.example.bobo.mydemo.R;
import com.example.bobo.mydemo.MyInterface;

import java.util.List;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private List<MyItem> itemList;
    private MyInterface listener;
    private static int sum = 0;


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_item_view,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final MyItem item = itemList.get(position);

        holder.myEditText1.updatePosition(position);

        holder.numberTextView.setText(String.valueOf(item.getValue()));
        holder.numberEditText.setText(String.valueOf(item.getValue()));
        holder.stringEditText.setText("");
    }

    public MyAdapter(List<MyItem> itemList, MyInterface listener) {
        this.itemList = itemList;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    private void updateSum(){
        sum = 0;
        for(int i = 0;i <itemList.size();i++){
            sum = sum+itemList.get(i).getValue();
        }
    }

    public int getSum(){
        updateSum();
        return sum;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView numberTextView, stringTextView;
        private EditText numberEditText, stringEditText;
        private MyEditText myEditText1, myEditText2;

        private MyViewHolder(View view){
            super(view);
            numberEditText = view.findViewById(R.id.numberEditText);
            numberTextView = view.findViewById(R.id.numberTextView);
            stringEditText = view.findViewById(R.id.stringEditText);
            stringTextView = view.findViewById(R.id.stringTextView);
            myEditText1 = new MyEditText(numberEditText,numberTextView);
            numberEditText.addTextChangedListener(myEditText1);
            myEditText2 = new MyEditText(stringEditText,stringTextView);
            stringEditText.addTextChangedListener(myEditText2);
        }
    }

    private class MyEditText implements TextWatcher{
        private int position;
        private View view;
        private TextView textView;

        MyEditText(View view, TextView textView) {
            this.view = view;
            this.textView = textView;
        }

        void updatePosition(int position){
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()){
                case R.id.numberEditText:
                    if (TextUtils.isEmpty(editable)) {
                        itemList.get(position).setValue(0);
                    }
                    else {
                        itemList.get(position).setValue(Integer.parseInt(editable.toString()));
                    }
                    listener.updateSum();
                    textView.setText(String.valueOf(itemList.get(position).getValue()));
                    listener.addUpdateList(new MyItem(String.valueOf(position),itemList.get(position).getValue()));
                    break;

                case R.id.stringEditText:
                    if(TextUtils.isEmpty(editable)){
                        textView.setText("");
                    }
                    else{
                        textView.setText(editable.toString());
                    }
                    break;
            }
        }
    }

}
