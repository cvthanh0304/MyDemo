package com.example.bobo.mydemo.UI.Activity;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.bobo.mydemo.Entity.Item;
import com.example.bobo.mydemo.Model.ItemModel;
import com.example.bobo.mydemo.R;
import com.example.bobo.mydemo.UI.Fragment.FragmentOne;

public class MainActivity extends AppCompatActivity {
    FragmentOne fragmentOne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inital();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container,fragmentOne);
        fragmentTransaction.commit();

        ItemModel itemModel = new ItemModel(this);
        if(itemModel.getItemsCount() == 0) {
            for (int i = 0; i < 1000; i++) {
                itemModel.addItem(new Item(String.valueOf(i), i));
            }
        }
    }

    private void inital(){
        fragmentOne = new FragmentOne();
    }
}
