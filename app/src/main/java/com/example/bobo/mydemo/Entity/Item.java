package com.example.bobo.mydemo.Entity;


public class Item {
    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "MyDatabase";

    public static final String MY_TABLE = "Table1";

    public static final String KEY_ID = "ID";
    public static final String KEY_VALUE = "value";

    private String ID;
    private int value;

    public Item(String ID, int value) {
        this.ID = ID;
        this.value = value;
    }

    public Item() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
