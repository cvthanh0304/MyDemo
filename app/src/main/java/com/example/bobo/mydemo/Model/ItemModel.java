package com.example.bobo.mydemo.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.bobo.mydemo.Entity.Item;

import java.util.ArrayList;
import java.util.List;


public class ItemModel extends SQLiteOpenHelper {

    private SQLiteDatabase db;

    public ItemModel(Context context){
        super(context, Item.DATABASE_NAME, null, Item.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE1 = "CREATE TABLE " + Item.MY_TABLE + "("
                + Item.KEY_ID + " TEXT PRIMARY KEY,"
                + Item.KEY_VALUE + " INTEGER" + ")";
        db.execSQL(CREATE_TABLE1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String DROP_TABLE1 = "DROP TABLE IF EXISTS" + Item.MY_TABLE;
        db.execSQL(DROP_TABLE1);
        onCreate(db);
    }

    public void addItem(Item item){
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Item.KEY_ID, item.getID());
        values.put(Item.KEY_VALUE,item.getValue());

        db.insert(Item.MY_TABLE, null,values);
        db.close();
    }

    public List<Item> getAllItems(){
        List<Item> result = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + Item.MY_TABLE;

        db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if(cursor.moveToNext()){
            do{
                Item item = new Item();
                item.setID(cursor.getString(0));
                item.setValue(cursor.getInt(1));
                result.add(item);
            }while(cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return result;
    }

    private int updateItem(Item item){
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Item.KEY_ID,item.getID());
        values.put(Item.KEY_VALUE,item.getValue());

        return db.update(Item.MY_TABLE, values, Item.KEY_ID + "="+item.getID(), null);
    }

    public Item getItem(String id){
        db = this.getReadableDatabase();

        Cursor cursor = db.query(Item.MY_TABLE, new String[]{ Item.KEY_ID, Item.KEY_VALUE},Item.KEY_ID +"= ?",new String[]{id},null,null,null,null);
        if(cursor != null)
            cursor.moveToFirst();

        Item result = new Item();
        result.setID(cursor.getString(0));
        result.setValue(cursor.getInt(1));

        db.close();
        return result;
    }

    public void updateAllItem(List<Item> list){
        db = this.getWritableDatabase();

        for(int i = 0;i<list.size();i++){
            updateItem(list.get(i));
        }

        db.close();
    }

    public int getItemsCount(){
        db = this.getReadableDatabase();
        String countQuery = "SELECT * FROM " + Item.MY_TABLE;
        Cursor cursor = db.rawQuery(countQuery,null);

        return cursor.getCount();
    }

    public void deleteAll(){
        db = this.getWritableDatabase();

        String deleteQuery = "DELETE FROM "+ Item.MY_TABLE;
        db.execSQL(deleteQuery);
        db.close();
    }
}
